#if __has_include(<React/RCTBridgeModule.h>)
#import <React/RCTBridgeModule.h>
#else
#import "RCTBridgeModule.h"
#endif

#if __has_include(<React/RCTEventEmitter.h>)
#import <React/RCTEventEmitter.h>
#else
#import "RCTEventEmitter.h"
#endif

#import <CallKit/CXCallObserver.h>
#import <CallKit/CXCall.h>

@interface RNCallManager : RCTEventEmitter <RCTBridgeModule, CXCallObserverDelegate>

@property (nonatomic, strong) CXCallObserver *callObserver;

@end
