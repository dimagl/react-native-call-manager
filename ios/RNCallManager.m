#import "RNCallManager.h"

@implementation RNCallManager

RCT_EXPORT_MODULE()


- (instancetype)init
{
    self = [super init];

    NSLog(@"Init");
    
    self.callObserver = [[CXCallObserver alloc] init];
    [self.callObserver setDelegate:self queue:nil];

    return self;
}

- (void)callObserver:(CXCallObserver *)callObserver callChanged:(CXCall *)call {
    
    if(call.hasEnded) {
        [self sendEventWithName:@"callEnded" body:@{}];
        NSLog(@"********** voice call disconnected **********/n");
        return;
    }
    
    if (call.hasConnected) {
        [self sendEventWithName:@"callConnected" body:@{}];
        NSLog(@"********** voice call connected **********/n");
        return;
    }
}

- (NSArray<NSString *> *)supportedEvents {
  return @[
      @"callEnded",
      @"callConnected",
  ];
}

- (void)startObserving {
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    for (NSString *notificationName in [self supportedEvents]) {
        [center addObserver:self
               selector:@selector(emitEventInternal:)
                   name:notificationName
                 object:nil];
    }
}

- (void)stopObserving {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (dispatch_queue_t)methodQueue
{
    return dispatch_get_main_queue();
}

RCT_EXPORT_METHOD(sampleMethod)
{
    NSLog(@"Log: sampleMethod");
    // TODO: Implement
}

@end
