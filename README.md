# react-native-call-manager

## Getting started

`$ npm install react-native-call-manager --save`

### Mostly automatic installation

`$ react-native link react-native-call-manager`

### Manual installation


#### iOS

1. In XCode, in the project navigator, right click `Libraries` ➜ `Add Files to [your project's name]`
2. Go to `node_modules` ➜ `react-native-call-manager` and add `RNCallManager.xcodeproj`
3. In XCode, in the project navigator, select your project. Add `libRNCallManager.a` to your project's `Build Phases` ➜ `Link Binary With Libraries`
4. Run your project (`Cmd+R`)<

#### Android

1. Open up `android/app/src/main/java/[...]/MainApplication.java`
  - Add `import com.uloop.callmanager.RNCallManagerPackage;` to the imports at the top of the file
  - Add `new RNCallManagerPackage()` to the list returned by the `getPackages()` method
2. Append the following lines to `android/settings.gradle`:
  	```
  	include ':react-native-call-manager'
  	project(':react-native-call-manager').projectDir = new File(rootProject.projectDir, 	'../node_modules/react-native-call-manager/android')
  	```
3. Insert the following lines inside the dependencies block in `android/app/build.gradle`:
  	```
      compile project(':react-native-call-manager')
  	```

#### Windows
[Read it! :D](https://github.com/ReactWindows/react-native)

1. In Visual Studio add the `RNCallManager.sln` in `node_modules/react-native-call-manager/windows/RNCallManager.sln` folder to their solution, reference from their app.
2. Open up your `MainPage.cs` app
  - Add `using Call.Manager.RNCallManager;` to the usings at the top of the file
  - Add `new RNCallManagerPackage()` to the `List<IReactPackage>` returned by the `Packages` method


## Usage
```javascript
import RNCallManager from 'react-native-call-manager';

// TODO: What to do with the module?
RNCallManager;
```
  