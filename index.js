import { NativeModules, NativeEventEmitter } from 'react-native';

const { RNCallManager } = NativeModules;
const invariant = require('invariant');

const CallManagerEmitter = new NativeEventEmitter(RNCallManager);

let CallManager = {
  /**
   * The `addListener` function connects a JavaScript function to an identified native
   * call notification event.
   *
   * This function then returns the reference to the listener.
   *
   * @param {string} eventName The `nativeEvent` is the string that identifies the event you're listening for.  This
   *can be any of the following:
   *
   * - `callEnded`
   * - `callConnected`
   *
   * @param {function} callback function to be called when the event fires.
   */
  addListener(eventName: CallEventName, callback: CallEventEventListener) {
    invariant(false, 'Dummy method used for documentation');
  },

  /**
   * Removes a specific listener.
   *
   * @param {string} eventName The `nativeEvent` is the string that identifies the event you're listening for.
   * @param {function} callback function to be called when the event fires.
   */
  removeListener(
    eventName: CallEventName,
    callback: CallEventEventListener,
  ) {
    invariant(false, 'Dummy method used for documentation');
  },

  /**
   * Removes all listeners for a specific event type.
   *
   * @param {string} eventType The native event string listeners are watching which will be removed.
   */
  removeAllListeners(eventName: CallEventName) {
    invariant(false, 'Dummy method used for documentation');
  },

  /**
   * Dismisses the active keyboard and removes focus.
   */
  sampleMethod() {
    invariant(false, 'Dummy method used for documentation');
  },
};

if (RNCallManager) {
  CallManager = CallManagerEmitter;
  CallManager.sampleMethod = RNCallManager.sampleMethod;
}

export default CallManager;
