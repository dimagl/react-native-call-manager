using ReactNative.Bridge;
using System;
using System.Collections.Generic;
using Windows.ApplicationModel.Core;
using Windows.UI.Core;

namespace Call.Manager.RNCallManager
{
    /// <summary>
    /// A module that allows JS to share data.
    /// </summary>
    class RNCallManagerModule : NativeModuleBase
    {
        /// <summary>
        /// Instantiates the <see cref="RNCallManagerModule"/>.
        /// </summary>
        internal RNCallManagerModule()
        {

        }

        /// <summary>
        /// The name of the native module.
        /// </summary>
        public override string Name
        {
            get
            {
                return "RNCallManager";
            }
        }
    }
}
